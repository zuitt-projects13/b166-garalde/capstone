const chai = require("chai");
const { expect, assert } = require("chai");
const http = require("chai-http");
chai.use(http);


describe("capstone test", () => {
  it("1. Check if post /currency is running", (done) => {
    chai
      .request("http://localhost:5000")
      .get("/currency")
      .end((error, response) => {
        assert.notEqual(response, undefined);
        done();
      });
  });

  it("2. Check if post /currency returns status 400 if name is missing", (done) => {
    chai
      .request("http://localhost:5000")
      .post("/currency")
      .type("json")
      .send({
        alias: "riyadh",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((error, response) => {
        expect(response.status).to.equal(400);
        done();
      });
  });

  it("3. Check if post /currency returns status 400 if name is not a string", (done) => {
    chai
      .request("http://localhost:5000")
      .post("/currency")
      .type("json")
      .send({
        alias: "riyadh",
        name: 123,
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((error, response) => {
        expect(response.status).to.equal(400);
        done();
      });
  });

  it("4. Check if post /currency returns status 400 if name is empty", (done) => {
    chai
      .request("http://localhost:5000")
      .post("/currency")
      .type("json")
      .send({
        alias: "riyadh",
        name: "",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((error, response) => {
        expect(response.status).to.equal(400);
        done();
      });
  });

  it("5. Check if post /currency returns status 400 if ex is missing", (done) => {
    chai
      .request("http://localhost:5000")
      .post("/currency")
      .type("json")
      .send({
        alias: "riyadh",
        name: "Saudi Arabian Riyadh"
      })
      .end((error, response) => {
        expect(response.status).to.equal(400);
        done();
      });
  });

  it("6. Check if post /currency returns status 400 if ex is not an object", (done) => {
    chai
      .request("http://localhost:5000")
      .post("/currency")
      .type("json")
      .send({
        alias: "riyadh",
        name: "Saudi Arabian Riyadh",
        ex: "ex",
      })
      .end((error, response) => {
        expect(response.status).to.equal(400);
        done();
      });
  });

  it("7. Check if post /currency returns status 400 if ex is empty", (done) => {
    chai
      .request("http://localhost:5000")
      .post("/currency")
      .type("json")
      .send({
        alias: "riyadh",
        name: "Saudi Arabian Riyadh",
        ex: {},
      })
      .end((error, response) => {
        expect(response.status).to.equal(400);
        done();
      });
  });

  it("8. Check if post /currency returns status 400 if alias is missing", (done) => {
    chai
      .request("http://localhost:5000")
      .post("/currency")
      .type("json")
      .send({
        name: "Saudi Arabian Riyadh",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((error, response) => {
        expect(response.status).to.equal(400);
        done();
      });
  });

  it("9. Check if post /currency returns status 400 if alias is not an string", (done) => {
    chai
      .request("http://localhost:5000")
      .post("/currency")
      .type("json")
      .send({
        alias: 123,
        name: "Saudi Arabian Riyadh",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((error, response) => {
        expect(response.status).to.equal(400);
        done();
      });
  });

  it("10. Check if post /currency returns status 400 if alias is empty", (done) => {
    chai
      .request("http://localhost:5000")
      .post("/currency")
      .type("json")
      .send({
        alias: "",
        name: "Saudi Arabian Riyadh",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((error, response) => {
        expect(response.status).to.equal(400);
        done();
      });
  });

  it("11. Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias", (done) => {
    chai
      .request("http://localhost:5000")
      .post("/currency")
      .type("json")
      .send({
          alias: "riyadh",
          name: "Saudi Arabian Riyadh",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((error, response) => {
        expect(response.status).to.equal(400);
        done();
      });
  });

  it("12. Check if post /currency returns status 200 if all fields are complete and there are no dupicates", (done) => {
    chai
      .request("http://localhost:5000")
      .post("/currency")
      .type("json")
      .send({
        alias: "PHP",
        name: "Philippine Peso",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((error, response) => {  
        expect(response.status).to.equal(200);
        done();
      });
  });
});
