let currencyDB = [
  {
    alias: "riyadh",
    name: "Saudi Arabian Riyadh",
    ex: {
      peso: 0.47,
      usd: 0.0092,
      won: 10.93,
      yuan: 0.065,
    },
  },
];

module.exports = (app) => {
  app.get("/currency", (req, res) => {
    //1. Check if post /currency is running
    return res.sendStatus(200);
  });

  app.post("/currency", (req, res) => {
    if (!req.body.hasOwnProperty("name")) {
      //2. Check if post /currency returns status 400 if name is missing
      return res.status(400).send("Error");
    }

    if (typeof req.body.name !== "string") {
      //3. Check if post /currency returns status 400 if name is not a string
      return res.status(400).send("Error");
    }

    if (req.body.name === "" || req.body.name === null) {
      //4. Check if post /currency returns status 400 if name is empty
      return res.status(400).send("Error");
    }

    if (!req.body.hasOwnProperty("ex")) {
      //5. Check if post /currency returns status 400 if ex is missing
      return res.status(400).send("Error");
    }

    if (typeof req.body.ex !== "object") {
      //6. Check if post /currency returns status 400 if ex is not an object
      return res.status(400).send("Error");
    }

    if (Object.entries(req.body.ex).length === 0) {
      //7. Check if post /currency returns status 400 if ex is empty
      return res.status(400).send("Error");
    }

    if (!req.body.hasOwnProperty("alias")) {
      //8. Check if post /currency returns status 400 if alias is missing
      return res.status(400).send("Error");
    }

    if (typeof req.body.alias !== "string") {
      //9. Check if post /currency returns status 400 if alias is not an string
      return res.status(400).send("Error");
    }

    if (req.body.alias === "" || req.body.alias === null) {
      //10. Check if post /currency returns status 400 if alias is empty
      return res.status(400).send("Error");
    }

    if (checkDuplicate(req.body)) {
      //11. Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias
      return res.status(400).send("Error");
    }

    // 12. Check if post /currency returns status 200 if all fields are complete and there are no dupicates
    //checking for complete fields are already processed above
    //checking for duplicate alias is already checked above

    // currencyDB.push(req.body);
    // console.log(currencyDB);
    return res.sendStatus(200);


    //check for duplicate alias
    function checkDuplicate(obj) {
      let newAlias = obj.alias;
      let withDuplicate = false;

      for (i = 0; i < currencyDB.length; i++) {
        if (currencyDB[i].alias === newAlias) {
          withDuplicate = true;
        }
      }
      return withDuplicate;
    }
  });
};
